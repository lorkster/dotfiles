alias textedit='open -a TextEdit'
alias tw='open -b com.barebones.textwrangler -n'
alias sutw='sudo open -b com.barebones.textwrangler -n'
alias gedit="open -a gedit"
alias ll="ls -GFhpl"
alias lla="ls -GFhpla"
alias cd..='cd ..'
alias tf='tail -F -n 200'
alias h=history
alias op='echo '\''User:      Command:   Port:'\''; echo '\''----------------------------'\'' ; sudo lsof -i 4 -P -n | grep -i '\''listen'\'' | awk '\''{print $3, $1, $9}'\'' | sed '\''s/ [a-z0-9\.\*]*:/ /'\'' | sort -k 3 -n |xargs printf '\''%-10s %-10s %-10s\n'\'' | uniq'

alias gz="tar -zcvf"
alias bz="tar -jcvf"
alias zp="zip"
alias ugz="tar -zxf"
alias ubz="tar -jxf"
alias utar="tar -xf"
alias uzp="unzip"
alias ltar="tar -tf"

alias java_ls='/usr/libexec/java_home -V 2>&1 | grep -E "\d.\d.\d_\d\d" | cut -d , -f 1 | colrm 1 4 | grep -v Home'

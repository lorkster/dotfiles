# Assume cloned into ~/dotfiles
rm ~/.bash_aliases
rm ~/.bash_profile
rm ~/.bashrc
rm ~/.gitcompletion.sh
rm ~/.gitconfig
rm ~/.gitignore_global
rm ~/.git_rmb
ln -s ~/dotfiles/.bash_aliases ~/.bash_aliases
ln -s ~/dotfiles/.bash_profile ~/.bash_profile
ln -s ~/dotfiles/.bashrc ~/.bashrc
ln -s ~/dotfiles/.gitcompletion.sh ~/.gitcompletion.sh
ln -s ~/dotfiles/.gitconfig ~/.gitconfig
ln -s ~/dotfiles/.gitignore_global ~/.gitignore_global
ln -s ~/dotfiles/.git_rmb ~/.git_rmb
. ~/.bash_profile

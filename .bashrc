function java_use() {
    export JAVA_HOME=$(/usr/libexec/java_home -v $1)
    export PATH=$JAVA_HOME/bin:$PATH
    java -version
}

java_use 1.7

[ -s "/Users/magnusdep/.scm_breeze/scm_breeze.sh" ] && source "/Users/magnusdep/.scm_breeze/scm_breeze.sh"
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

if [ -f ~/.bashrc ]; then 
    source ~/.bashrc 
fi

if [ -f ~/.gitcompletion.sh ]; then
    source ~/.gitcompletion.sh
fi

ulimit -n 200000
export HISTSIZE=5000
export HISTFILESIZE=5000
export HISTCONTROL=ignoredups:erasedups

export GLASSFISH_HOME=/usr/local/opt/glassfish/libexec
export PATH=${PATH}:${GLASSFISH_HOME}/bin
export GRAILS_HOME=/usr/local/opt/grails/libexec
export MAVEN_OPTS='-Xmx3072m -Xms1024m -XX:MaxPermSize=512M -Xdebug -Xrunjdwp:transport=dt_socket,address=4000,server=y,suspend=n'
export SCALA_HOME=/usr/local/Cellar/scala/2.11.1/libexec
export SBT_OPTS="-XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:PermSize=128M -XX:MaxPermSize=512M"

# Manually source autojump - https://github.com/joelthelion/autojump
[[ -s `brew --prefix`/etc/autojump.sh ]] && . `brew --prefix`/etc/autojump.sh

#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
[[ -s "/Users/magnusdep/.gvm/bin/gvm-init.sh" ]] && source "/Users/magnusdep/.gvm/bin/gvm-init.sh"
